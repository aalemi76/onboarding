//
//  MessageCollectionViewCell.swift
//  OnBoarding
//
//  Created by a.alami on 27/08/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class MessageCollectionViewCell: UICollectionViewCell {
    static let reuseID = "MessageCollectionViewCell"
    private let textLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18, weight: .heavy)
        label.textColor = GlobalSettings.shared().white
        label.adjustsFontSizeToFitWidth = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    private let blurEffect: UIBlurEffect = {
        let effect = UIBlurEffect(style: .light)
        return effect
    }()
    private let mainView = UIView()
    private let cornerRadius: CGFloat = 10
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.sizeToFit()
        contentView.layoutIfNeeded()
        addViewConstraint()
        addBlurredView()
        loadView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configureView() {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    private func addBlurredView() {
        let blurredView = UIVisualEffectView(effect: blurEffect)
        blurredView.layer.cornerRadius = cornerRadius
        blurredView.layer.masksToBounds = true
        blurredView.clipsToBounds = true
        blurredView.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(blurredView)
        NSLayoutConstraint.activate([
            blurredView.topAnchor.constraint(equalTo: mainView.topAnchor),
            blurredView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            blurredView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            blurredView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor)])
    }
    private func addViewConstraint() {
        mainView.clipsToBounds = true
        mainView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(mainView)
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            mainView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor),
            mainView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor)])
        let widthConstraint = mainView.widthAnchor.constraint(greaterThanOrEqualToConstant: 30)
        widthConstraint.priority = UILayoutPriority(249)
        widthConstraint.isActive = true
        let heightConstraint = mainView.heightAnchor.constraint(greaterThanOrEqualToConstant: 50)
        heightConstraint.priority = UILayoutPriority(249)
        heightConstraint.isActive = true
    }
    private func loadView() {
        let indicator = addTypingIndicator()
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseInOut, .transitionCrossDissolve], animations: {
            indicator.alpha = 1
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                indicator.removeFromSuperview()
                UIView.animate(withDuration: 2, delay: 0, options: [], animations: {
                    self.addText()
                    self.textLabel.alpha = 1
                    }, completion: nil)
            }
        }
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseInOut, .transitionCrossDissolve], animations: {
            indicator.alpha = 1
        }, completion: nil)
    }
    private func addText() {
        textLabel.alpha = 0
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(textLabel)
        NSLayoutConstraint.activate([
            textLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
            textLabel.topAnchor.constraint(greaterThanOrEqualTo: mainView.topAnchor, constant: 10),
            textLabel.bottomAnchor.constraint(greaterThanOrEqualTo: mainView.bottomAnchor, constant: -10),
            textLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -15)])
    }
    @discardableResult private func addTypingIndicator() -> UIView {
        let indicator = TypingIndicatorView(receiverName: "SportMob")
        indicator.alpha = 0
        indicator.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            indicator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            indicator.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor),
            indicator.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor)])
        return indicator
    }
    func setWidthConstraint(constant: CGFloat) {
        contentView.widthAnchor.constraint(equalToConstant: constant).isActive = true
        textLabel.widthAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
    }
    func setTextLabel(text: String) {
        self.textLabel.text = text.uppercased()
    }
}
