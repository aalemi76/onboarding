//
//  TeamCollectionViewCell.swift
//  OnBoarding
//
//  Created by a.alami on 27/08/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class TeamCollectionViewCell: UICollectionViewCell {
    static let reuseID = String(describing: TeamCollectionViewCell.self)
    private let image = UIImageView()
    private let blurEffect: UIBlurEffect = {
        let effect = UIBlurEffect(style: .light)
        return effect
    }()
    private let cornerRadius: CGFloat = 10
    override var isSelected: Bool {
        willSet {
            layer.borderColor = newValue ? GlobalSettings.shared().yellow.cgColor : GlobalSettings.shared().lightGray.cgColor
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        layer.borderWidth = 1
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        layer.borderColor = GlobalSettings.shared().lightGray.cgColor
        addBlurredView()
        addImage()
    }
    private func addImage() {
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        addSubview(image)
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            image.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            image.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            image.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
    }
    private func addBlurredView() {
        let blurredView = UIVisualEffectView(effect: blurEffect)
        blurredView.layer.cornerRadius = cornerRadius
        blurredView.layer.masksToBounds = true
        blurredView.clipsToBounds = true
        blurredView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(blurredView)
        NSLayoutConstraint.activate([
            blurredView.topAnchor.constraint(equalTo: topAnchor),
            blurredView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurredView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurredView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    func setImage(named: String) {
        image.image = UIImage(named: named)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
