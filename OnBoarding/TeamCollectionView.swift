//
//  TeamCollectionView.swift
//  OnBoarding
//
//  Created by a.alami on 27/08/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class TeamCollectionView: UICollectionViewCell {
    static let reuseID = String(describing: TeamCollectionView.self)
    let items = ["search", "10260_small", "liectercity", "chelsea", "arsenal", "8586_small", "inter", "10260_small", "liectercity", "chelsea", "arsenal", "8586_small", "inter"]
    var collectionView: UICollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        contentView.sizeToFit()
        contentView.layoutIfNeeded()
        configureView()
        configureCollectionView()
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseInOut, .transitionCrossDissolve], animations: {
            self.collectionView.alpha = 1
            self.layoutIfNeeded()
        }, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configureView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 75, height: 75)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 30
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.alpha = 0
        collectionView.allowsMultipleSelection = true
        collectionView.backgroundColor = .clear
        collectionView.register(TeamCollectionViewCell.self, forCellWithReuseIdentifier: TeamCollectionViewCell.reuseID)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: 120)])
        collectionView.layoutIfNeeded()
        collectionView.sizeToFit()
    }
    func setWidthConstraint(constant: CGFloat) {
        contentView.widthAnchor.constraint(equalToConstant: constant).isActive = true
    }
}
extension TeamCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TeamCollectionViewCell.reuseID, for: indexPath) as! TeamCollectionViewCell
        cell.setImage(named: items[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row == 0 else { return }
        collectionView.deselectItem(at: indexPath, animated: false)
    }
}
