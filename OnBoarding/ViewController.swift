//
//  ViewController.swift
//  OnBoarding
//
//  Created by a.alami on 27/08/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    var collectionView: UICollectionView!
    let texts = ["Hello Buddy", "Good Morning", "Here we want to have fun together, so let’s get to know each other", "Choose your favorite team(s)", "Team Selection", "One Final Question!"]
    var counter = 0
    var items = [String]()
    var constant: CGFloat!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        constant = view.bounds.height - 20
        configureCollectionView()
        sendText()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
            self.sendText()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 9) {
            self.sendText()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 13.5) {
            self.sendText()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 16) {
            self.sendText()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            self.sendText()
        }
    }
    func configureBackground() -> UIView {
        let mainView = UIView()
        let image = UIImageView(image: UIImage(named: "bag-policy"))
        image.contentMode = .scaleAspectFill
        setConstraints(mainView, subView: image)
        let colorView = UIView()
        colorView.backgroundColor = GlobalSettings.shared().mainColor
        colorView.alpha = 0.5
        setConstraints(mainView, subView: colorView)
        return mainView
    }
    func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 20
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MessageCollectionViewCell.self, forCellWithReuseIdentifier: MessageCollectionViewCell.reuseID)
        collectionView.register(TeamCollectionView.self, forCellWithReuseIdentifier: TeamCollectionView.reuseID)
        collectionView.backgroundView = configureBackground()
        setConstraints(view, subView: collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    func setConstraints(_ superView: UIView, subView: UIView) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        superView.addSubview(subView)
        NSLayoutConstraint.activate([
            subView.topAnchor.constraint(equalTo: superView.topAnchor),
            subView.leadingAnchor.constraint(equalTo: superView.leadingAnchor),
            subView.trailingAnchor.constraint(equalTo: superView.trailingAnchor),
            subView.bottomAnchor.constraint(equalTo: superView.bottomAnchor)])
    }
    func createText() {
        items.append(texts[counter])
    }
    func sendText() {
        guard counter <= texts.count else { return }
        createText()
        collectionView.reloadData()
        constant -= 100
        let indexPath = IndexPath(row: counter, section: 0)
        scrollToItem()
        counter += 1
    }
    func scrollToItem() {
        let collectionViewContentHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
        let isContentTooSmall: Bool = (collectionViewContentHeight < self.collectionView.bounds.size.height)
        if isContentTooSmall {
            self.collectionView.scrollRectToVisible(CGRect(x: 0, y: collectionViewContentHeight - 50, width: 1, height: 1), animated: true)
            return
        }
        self.collectionView.scrollToItem(at: NSIndexPath(item: items.count - 1, section: 0) as IndexPath, at: .bottom, animated: true)
    }
}
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: constant, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TeamCollectionView.reuseID, for: indexPath) as! TeamCollectionView
            cell.setWidthConstraint(constant: collectionView.bounds.width - 50)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MessageCollectionViewCell.reuseID, for: indexPath) as! MessageCollectionViewCell
            let item = items[indexPath.row]
            cell.setTextLabel(text: item)
            cell.setWidthConstraint(constant: collectionView.bounds.width - 50)
            return cell
        }
    }
}
