//
//  GlobalSettings.swift
//  OnBoarding
//
//  Created by a.alami on 27/08/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
final class GlobalSettings: NSObject {
    private static var sharedInstance: GlobalSettings = {
        let globalSettings = GlobalSettings()
        return globalSettings
    }()
    class func shared() -> GlobalSettings {
        return sharedInstance
    }
    let mainColor = UIColor(displayP3Red: 3/255, green: 26/255, blue: 77/255, alpha: 1)
    let lightGray = UIColor(displayP3Red: 237/255, green: 238/255, blue: 240/255, alpha: 1)
    let darkGray = UIColor(displayP3Red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
    let white = UIColor(displayP3Red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
    let yellow = UIColor(displayP3Red: 255/255, green: 204/255, blue: 0, alpha: 1)
}
